#!/bin/sh -e

VERSION=$2
TAR=../xsom_$VERSION.orig.tar.xz
DIR=xsom-$VERSION
TAG=$(echo "xsom-$VERSION" | sed -re's/0\+//')

svn export https://svn.java.net/svn/xsom~sources/tags/${TAG}/ $DIR
XZ_OPT=--best tar -c -J -f $TAR \
    --exclude '*.jar' \
    --exclude '*.class' \
    --exclude '*.ipr' \
    --exclude '*.iml' \
    --exclude '.settings' \
    --exclude '.project' \
    --exclude '.classpath' \
    --exclude 'lib' \
    --exclude 'src/com/sun/xml/xsom/impl/scd/ParseException.java' \
    --exclude 'src/com/sun/xml/xsom/impl/scd/SCDParser.java' \
    --exclude 'src/com/sun/xml/xsom/impl/scd/SCDParserConstants.java' \
    --exclude 'src/com/sun/xml/xsom/impl/scd/SCDParserTokenManager.java' \
    --exclude 'src/com/sun/xml/xsom/impl/scd/SimpleCharStream.java' \
    --exclude 'src/com/sun/xml/xsom/impl/scd/Token.java' \
    --exclude 'src/com/sun/xml/xsom/impl/scd/TokenMgrError.java' \
    $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir && echo "moved $TAR to $origDir"
fi
